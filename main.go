package main

import (
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"regexp"
	_ "embed"
)

func validate_ip(ip string) (string, error) {

	re := regexp.MustCompile(`^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$`)

	if re.MatchString(ip) {
		return ip, nil
	} else {
		return ip, errors.New(fmt.Sprintf("\"%s\" is not a valid IP", ip))
	}

}

func validate_locked(locked string) (bool, error) {
	re := regexp.MustCompile(`^(true|false)$`)
	if re.MatchString(locked) {
		if locked == "true" {
			return true, nil
		} else {
			return false, nil
		}
	} else {
		return false, errors.New(fmt.Sprintf("\"%s\" is not valid status", locked))
	}
}

func save_file(data map[string]bool, filename string) error {
	as_json, err := json.Marshal(data)
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(filename, as_json, 0644)
	if err != nil {
		return err
	}
	return nil
}

func bool_string(x bool) string {
	if x {
		return "locked"
	} else {
		return "unlocked"
	}
}

//go:embed web/templates/index.html.templ
var index_html string

func main() {
	var port string
	var filepath string
	flag.StringVar(&port, "p", "8080", "Specify the port to listen on")
	flag.StringVar(&filepath, "f", "resources", "Specify where to save for persistence")
	flag.Parse()

	resources := make(map[string]bool)
	var err error

	content, err := ioutil.ReadFile(filepath)
	if err != nil {
		if os.IsNotExist(err) {
			os.Create(filepath)
			log.Printf("Creating file: %s\n", filepath)
		} else {
			log.Fatal(err)
		}
	}
	if len(content) > 0 {
		err = json.Unmarshal(content, &resources)
		if err != nil {
			log.Fatal(err)
		}
	}

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		var err error

		t, err := template.New("webpage").Parse(index_html)
		if err != nil {
			log.Fatal(err)
			w.WriteHeader(http.StatusNotFound)
		}

		data := struct {
			Items map[string]bool
		}{
			Items: resources,
		}

		err = t.Execute(w, data)
		if err != nil {
			log.Fatal(err)
			w.WriteHeader(http.StatusNotFound)
		}

	})

	http.HandleFunc("/lock", func(w http.ResponseWriter, r *http.Request) {
		var locked bool = false
		var ipaddr string
		var err error

		w.Header().Set("Content-Type", "text/plain; charset=utf-8") // normal header
		if locked, err = validate_locked(r.FormValue("locked")); err != nil {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(fmt.Sprintf("%s", err)))
			return
		}

		if ipaddr, err = validate_ip(r.FormValue("ip")); err != nil {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(fmt.Sprintf("%s", err)))
			return
		}

		if !locked {
			resources[ipaddr] = false
			if err = save_file(resources, filepath); err != nil {
				log.Fatal("Could not write to")
			}
			return
		} else if resources[ipaddr] && locked {
			w.WriteHeader(http.StatusLocked)
			return
		} else if !resources[ipaddr] && locked {
			resources[ipaddr] = true
			if err = save_file(resources, filepath); err != nil {
				log.Fatal(err)
			}
			return
		}
		w.WriteHeader(http.StatusBadRequest)
	})
	if err = save_file(resources, filepath); err != nil {
		log.Fatal(err)
	}
	http.ListenAndServe(fmt.Sprintf(":%s", port), nil)

}
